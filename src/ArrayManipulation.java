import java.util.Arrays;
public class ArrayManipulation {
    static int numbers[] = { 5, 8, 3, 2, 7 };
    static String names[] = { "Alice", "Bob", "Charlie", "David" };
    static double values[] = new double[4];

    public static void main(String[] args) {
        printNumbers();
        printNames();
        initializeValues();
        calculateValues();
        findMaximumValues();
        reversedNames();
        sortNumbers();

    }

    public static void printNumbers() {
        for (int i = 0; i < 5; i++) {
            System.out.print(numbers[i] + " ");
        }System.out.println();
    }


    public static void printNames() {
        for (String i : names) {
            System.out.print(i + " ");
        }System.out.println();
    }

    public static void initializeValues() {
        values[0] = 1.5;
        values[1] = 6.4;
        values[2] = 3.4;
        values[3] = 4.01;
    }

    public static void calculateValues(){
        double sum = 0.00;
        for (int i = 0; i < 4; i++) {
            sum += values[i];
        }System.out.println(sum);
    }

    public static void findMaximumValues() {
        double max = values[0];
        for (int i = 1; i < 4; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }System.out.println(max);
    }

    public static void reversedNames() {
        int namesSize = names.length;
        String reversedNames[] = new String[namesSize];
        for (int i = 1; i <= namesSize; i++) {
            reversedNames[i - 1] = names[namesSize - i];
        }System.out.println(Arrays.toString(reversedNames));
    }

    public static void sortNumbers() {
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

}
